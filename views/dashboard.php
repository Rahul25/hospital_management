<?php
include_once('../vendor/autoload.php');
use App\Admin\Auth;

$auth = new Auth();

$total_inpatient = $auth->countPatient();
//var_dump($total_inpatient);

$total_doctor = $auth->countDoctor();

$total_released = $auth->countReleased();
//var_dump($total_released);
//die();
$total_dept = $auth->countDept();
$total_room = $auth->countRoom();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>add dept</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome_admin.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="room_list.php">All Room</a>
            <li><a href="all_doctor.php">All Doctor</a></li>
            <li><a href="all_released.php">All Released</a></li>
            <li><a href="menu_list.php">All Menu</a></li>
            <li><a href="dept_list.php">All Dept</a></li>
    </div>
    </form></li>

    </ul>
    </div>
</nav>

<div class="container">
    <div class="jumbotron" style="background-color:#11866f;color: #FFFFFF;padding-top: 0px;padding-bottom: 0px; ;padding-right: 250px;padding-left: 250px; ">
        <h2 align="center">InPatient</h2>
        <div class="jumbotron" style="background-color: #269abc;color: #FFFFFF;padding-left:10px;padding-right: 10px;padding-top: 10px;padding-bottom: 10px;">
            <h2 align="center"><?php echo $total_inpatient['total_inpatient']?></h2>
        </div>
        <p align="center"><a href="all_patient.php" style="color: #FFFFFF">view details</a></p>
    </div>

    <div class="jumbotron" style="background-color:#11866f;color: #FFFFFF;padding-top: 0px;padding-bottom: 0px; ;padding-right: 250px;padding-left: 250px; ">
        <h2 align="center">Doctor</h2>
        <div class="jumbotron" style="background-color: #269abc;color: #FFFFFF;padding-left:10px;padding-right: 10px;padding-top: 10px;padding-bottom: 10px;">
            <h2 align="center"><?php echo $total_doctor['total_doctor']?></h2>
        </div>
        <p align="center"><a href="all_doctor.php" style="color: #FFFFFF">view details</a></p>
    </div>

    <div class="jumbotron" style="background-color:#11866f;color: #FFFFFF;padding-top: 0px;padding-bottom: 0px; ;padding-right: 200px;padding-left: 200px; ">
        <h2 align="center">Released Patient</h2>
        <div class="jumbotron" style="background-color: #269abc;color: #FFFFFF;padding-left:10px;padding-right: 10px;padding-top: 10px;padding-bottom: 10px;">
            <h2 align="center"><?php echo $total_released['total_released']?></h2>
        </div>
        <p align="center"><a href="all_released.php" style="color: #FFFFFF">view details</a></p>
    </div>

    <div class="jumbotron" style="background-color:#11866f;color: #FFFFFF;padding-top: 0px;padding-bottom: 0px; ;padding-right: 250px;padding-left: 250px; ">
        <h2 align="center">Department</h2>
        <div class="jumbotron" style="background-color: #269abc;color: #FFFFFF;padding-left:10px;padding-right: 10px;padding-top: 10px;padding-bottom: 10px;">
            <h2 align="center"><?php echo $total_dept['total_dept']?></h2>
        </div>
        <p align="center"><a href="dept_list.php" style="color: #FFFFFF">view details</a></p>
    </div>

    <div class="jumbotron" style="background-color:#227b59;color: #FFFFFF;padding-top: 0px;padding-bottom: 0px; ;padding-right: 250px;padding-left: 250px; ">
        <h2 align="center">Room</h2>
        <div class="jumbotron" style="background-color: #269abc;color: #FFFFFF;padding-left:10px;padding-right: 10px;padding-top: 10px;padding-bottom: 10px;">
            <h2 align="center"><?php echo $total_room['total_room']?></h2>
        </div>
        <p align="center"><a href="room_list.php" style="color: #FFFFFF">view details</a></p>
    </div>


</div>
<footer class="text-center" style="margin-top: 100px">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
</html>