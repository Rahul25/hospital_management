<?php
session_start();
include_once ('../vendor/autoload.php');
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;
use App\User\Auth;
$ob= new App\Admin\Auth();
$allitem=$ob->menuList();
//var_dump($allItem);
//var_dump($_SESSION['user_email']);
//die();
$auth= new Auth();
$status=$auth->is_loggedin();
if($status== FALSE){
    Message::message("<div class=\"alert alert-danger\">
<strong>Taken!</strong> You have to log in before view this page
</div>");
    return Utility::redirect('../index.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hospital Management System</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]--></head>
<body id="page-top" class="index">

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->

        <!--     <h1 align="center" color="#fff">Hospital Management System</h1>-->
        <div class="navbar-header page-scroll">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>



            </button>
            <a class="navbar-brand" href="#page-top" style="margin-top: -10px;margin-left: -69px"href="#page-top"><img src="../Resources/welcome/img/logo.PNG" width="100" height="40"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" id="navbarmenu">
                <!--<li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li class="page-scroll">
                    <a href="welcome.php">Home</a>
                </li>-->
               <!-- <li class="page-scroll">
                    <a href="doctor_info.php"><?php //echo $item[0]['menus']?></a>
                </li>
               <li class="page-scroll">
                    <a href="patient_info.php"><?php //echo $item[1]['menus']?></a>
                </li>
                <li class="page-scroll">
                    <a href="released_patient.php"><?php //echo $item[2]['menus']?></a>
                </li>
                <li class="page-scroll">
                    <a href="view_profile.php?email=<?php //echo $_SESSION['user_email'] ?>"><?php //echo $item[3]['menus']?></a>
                </li>
                <li class="page-scroll">
                    <a href="Authenticate/logout.php"><?php //echo $item[4]['menus']?></a>
                </li>-->
                <?php foreach($allitem as $item){?>
                    <li class="page-scroll">
                        <a href="<?php echo $item['link'] ?>?email=<?php echo $_SESSION['user_email']?>"><?php echo $item['menus']?></a>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Header -->
<header style="min-height:870px">

    <div class="container">
        <div id="message">
            <?php
            if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
                echo Message::message();
            }
            ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="../Resources/welcome/img/profile.png" alt="">
                <div class="intro-text" style="margin-top: 105px;">
                    <span class="name" style="margin-top:50px">Welcome To Our Hospital</span>
                    <hr class="star-light" style="margin-top: 80px;">


                    <div class="row" id="margingTop" style="margin-top:50px">
                        <div class="col-lg-12">
                            <a href="set_profile.php?email=<?php echo $_SESSION['user_email'] ?>" class="btn btn-success">Set Profile</a>
                            <a href="add_patient.php" class="btn btn-success">Add patient</a>
                            <a href="release_patient.php" class="btn btn-success">Release Patient</a>
                            <a href="add_doctor.php" class="btn btn-success">Add Doctor</a>
                            <a href="rroom_list.php" class="btn btn-success">Room List </a>
                            <a href="rdept_list.php" class="btn btn-success">Dept List </a>

                        </div>
                    </div>





















                    <!--<span class="skills">Best Treatment - Health & Life Service - Qualified Medical Service</span>-->
                </div>
            </div>
        </div>
    </div>
</header>





<footer class="text-center">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="../Resources/js/index.js"></script>

<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>


</html>