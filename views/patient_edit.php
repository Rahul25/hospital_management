<?php
include_once('../vendor/autoload.php');
use App\Patient\Patient;
use App\Doctor\Doctor;
use App\Utility\Utility;

$patient= new Patient();
$singleItem=$patient->prepare($_GET)->view();
//Utility::dd($singleItem);
$doctor= new Doctor();
$allItem=$doctor->doctorList();
$allRoom=$doctor->roomList();

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--<style>
                        tr:nth-child(even) {background: #ccc}
                        tr:nth-child(odd) {background: #ccc}
                    </style>-->
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="add_patient.php">Add Patient</a></li>
            <li><a href="patient_info.php">Doctor Info</a></li>
            <li><a href="patient_info.php">Patient Info</a></li>
            <li><a href="released_patient.php">Released Patient</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <h2 class="jumbotron" align="center" style="padding-top: 10px;padding-bottom: 10px;color: #FFFFFF;background-color: #11866f">Edit Patient</h2>
    <div class="jumbotron" style=" background-color:#11866f;padding-right: 150px;padding-left: 150px;color: #FFFFFF">

    <form role="form" action="patient_update.php" method="post">
        <div class="form-group">
            <label>Patient Id:</label>
            <!--<input type="hidden" name="patient_id"  value="<?php echo $singleItem->id?>">-->
            <input type="text" name="patient_id" class="form-control" value="<?php echo $singleItem->patient_id?>">
        </div>
        <div class="form-group">
            <label>Patient Name:</label>
            <input type="text" class="form-control"name="patient_name" value="<?php echo $singleItem->patient_name?>">
        </div>
        <div class="form-group">
            <label>Mobile No:</label>
            <input type="text" class="form-control"name="mobile" value="<?php echo $singleItem->mobile?>">
        </div>
        <div class="form-group">
            <label>Disease:</label>
            <input type="text" class="form-control"name="disease" value="<?php echo $singleItem->disease?>">
        </div>
        <div class="form-group">
            <label for="sel1">Select Room:</label>
            <select class="form-control" id="sel1" name="room_id">
                <?php foreach($allRoom as $item){?>
                    <option value="<?php echo $item['room_id']?>" <?php if($singleItem->room_id ==$item['room_no'])echo "selected";?>><?php echo $item['room_no']?></option>
                <?php } ?>
            </select>
        </div>

        <div class="form-group">
            <label>Entry Date:</label>
            <input type="date" class="form-control"name="entry_date" value="<?php echo $singleItem->entry_date?>">
        </div>
        <div class="form-group">
            <label for="sel1">Edit Doctor:</label>
            <select class="form-control" id="sel1" name="doctor_id">
                <?php foreach($allItem as $item){?>
                    <option value="<?php echo $item['doctor_id']?>"<?php if($singleItem->doctor_id ==$item['doctor_id'])echo "selected";?>><?php echo $item['doctor_name']?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="sel1">Select Your Gender:</label>
            <select class="form-control" id="sel1" name="gender">
                <option value="male" <?php if($singleItem->gender == "male")echo "selected";?>>Male</option>
                <option value="female" <?php if($singleItem->gender == "female")echo "selected";?>>Female</option>
                <option value="others" <?php if($singleItem->gender == "others")echo "selected";?>>Others</option>
            </select>
        </div>
        <div class="form-group">
            <label>Age:</label>
            <input type="text" class="form-control"name="age" value="<?php echo $singleItem->age?>">
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>
</div>
<footer class="text-center" style="margin-top: 180px">
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
</html>