<?php
require_once('../vendor/mpdf/mpdf/mpdf.php');
include_once('../vendor/autoload.php');
use App\Patient\Patient;
use App\Doctor\Doctor;
use App\Message\Message;
use App\Utility\Utility;
$patient=new Patient();
$doctor=new doctor();
//var_dump($_POST);
//die();
$patient->prepare($_POST)->release_store();
$singlePatient=$patient->prepare($_POST)->view();
$Doctor_Id['doctor_id']=$singlePatient->doctor_id;
//var_dump($Doctor_Id);
//die();
$Room_Id['room_id']=$singlePatient->room_id;
$singleDoctor=$doctor->prepare($Doctor_Id)->view();
$singleRoom=$patient->prepare($Room_Id)->room_no();
$trs="";
$trs.="<ul>";
$trs.="<li>ID: <?php echo $singlePatient->patient_id ?></li>";
$trs.="<li>Patient Name: <?php echo $singlePatient->patient_name ?></li>";
$trs.="<li>Doctor Name: <?php echo $singleDoctor->doctor_name ?></li>";
$trs.="<li>Disease: <?php echo $singlePatient->disease ?></li>";
$trs.="<li>Room No: <?php echo $singleRoom->room_no ?></li>";
$trs.="<li>Gender: <?php echo $singlePatient->gender ?></li>";
$trs.="<li>Age: <?php echo $singlePatient->age ?></li>";
$trs.="<li>Release Date: <?php echo $singlePatient->release_date ?></li>";
$trs.="<li>Medicine Bill: <?php echo $singlePatient->medicine_bill ?></li>";
$trs.="<li>Room Bill: <?php echo $singlePatient->room_bill ?></li>";
$trs.="<li>Doctor Bill: <?php echo $singlePatient->doctor_bill ?></li>";
$trs.="<li>Total Bill: <?php echo $singlePatient->medicine_bill+$singlePatient->room_bill+$singlePatient->doctor_bill ?></li>";
$trs.="</ul>";
$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($trs);

// Output a PDF file directly to the browser
$mpdf->Output('bill.pdf','D');