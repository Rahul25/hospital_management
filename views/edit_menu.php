<?php
include_once('../vendor/autoload.php');
use App\Admin\Auth;
$auth= new Auth();
$item=$auth->prepare($_GET)->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Doctor Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--<style>
                    tr:nth-child(even) {background: #ccc}
                    tr:nth-child(odd) {background: #ccc}
                </style>-->
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome_admin.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="add_menu.php">Add Menu</a>
            <li><a href="all_doctor.php">All Doctor</a></li>
            <li><a href="all_released.php">All Released</a></li>
            <li><a href="room_list.php">All Room</a></li>
            <li><a href="dept_list.php">All Dept</a></li>
    </div>
    </form></li>

    </ul>
    </div>
</nav>

<div class="container">
    <h2 class="jumbotron" align="center" style="padding-top: 10px;padding-bottom: 10px;color: #FFFFFF;background-color: #11866f">Edit Menu</h2>
    <div class="jumbotron" style=" background-color:#11866f;padding-right: 150px;padding-left: 150px;color: #FFFFFF">

    <form role="form" action="update_menu.php" method="post">
        <div class="form-group">
            <label>Menu:</label>
            <input type="hidden" name="menu_id"  value="<?php echo $_GET['menu_id']?>">
            <input type="text" name="menu" class="form-control" value="<?php echo $item->menus?>">
        </div>
        <div class="form-group">
            <label>Link:</label>
            <input type="text" name="link" class="form-control" value="<?php echo $item->link?>">
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>
</div>
<footer class="text-center" style="margin-top: 180px">
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
</html>
