<?php
session_start();
include_once ('../vendor/autoload.php');
//var_dump($_POST);


use App\Patient\Patient;
use App\Utility\Utility;
use App\Message\Message;
$patient = new Patient();
$allName= $patient->allName();
$allRoom= $patient->allRoom();
//$allinfo=$book->allTD();
//Utility::dd($allinfo);
$commaSeparated= implode('","',$allName);
$commaSeparatedString= implode('","',$allRoom);
//$commaSeparatedStrings= implode('","',$allinfo);

//Utility::dd($commaSeparatedString);
$allTD= '"'.$commaSeparated.$commaSeparatedString.'"';

$totalItem=$patient->release_count();
//Utility::dd($totalItem);
if(array_key_exists('itemPerPage',$_SESSION)) {
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];
    }

}
else{
    $_SESSION['itemPerPage']=5;
}
$itemPerPage= $_SESSION['itemPerPage'];


$noOfPage=ceil($totalItem/$itemPerPage);
//Utility::d($noOfPage);
$pagination="";
if(array_key_exists('pageNo',$_GET)){
    $pageNo= $_GET['pageNo'];
}else {
    $pageNo = 1;
}
for($i=1;$i<=$noOfPage;$i++){
    $active=($i==$pageNo)?"active":"";
    $pagination.="<li class='$active'><a href='released_patient.php?pageNo=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNo-1);
if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) {
    $allInfo = $patient->release_paginator($pageStartFrom, $itemPerPage);
}
if(strtoupper($_SERVER['REQUEST_METHOD']=='POST')) {
    //Utility::dd($_POST);
    //die();
    $allInfo = $patient->prepare($_POST)->index();
}
if(strtoupper(($_SERVER['REQUEST_METHOD']=='GET')) && isset($_GET['search'])) {
    $allInfo = $patient->prepare($_GET)->index();
}


?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <style>
        tr:nth-child(even) {background: #ccc}
        tr:nth-child(odd) {background: #ccc}
    </style>
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="add_doctor.php">Add Doctor</a></li>
            <li><a href="add_patient.php">Add Patient</a></li>
            <li><a href="patient_info.php">Patient Info</a></li>
            <li><a href="doctor_info.php">Doctors</a></li>
            <!--<li><a href="Authenticate/logout.php">Log Out</a></li>-->
            <li><form style="margin-top: 13px" role="form" action="released_patient.php" method="get">
                    <?php if(isset($_GET['search'])){
                        $catchSearch=$_GET['search'];
                    }
                    ?>
                    <div class="form-group">
                        <input type="text" name="search"placeholder="Search" value="<?php if(isset($_GET['search'])){echo $catchSearch;}?>" id="search">
                        <button type="submit" class="glyphicon glyphicon-search"></button>
                    </div>

                </form></li>

            <li> <form role="form" action="released_patient.php" style="margin-top: 13px;padding-left: 50px" >
                    <div class="form-group">
                        <select class="form-control" id="sel1" name="itemPerPage" onchange="this.form.submit()">
                            <option <?php if($itemPerPage==5) echo "selected"?>>5</option>
                            <option <?php if($itemPerPage==10) echo "selected"?>>10</option>
                            <option <?php if($itemPerPage==15) echo "selected"?>>15</option>
                            <option <?php if($itemPerPage==20) echo "selected"?>>20</option>
                            <option <?php if($itemPerPage==25) echo "selected"?>>25</option>
                        </select>
                        <!-- <button type="submit">GO!</button>-->

                    </div>
                </form></li>

        </ul>
    </div>
</nav>

<div class="container">
    <h2 class="jumbotron" align="center">All Released Patient</h2>

    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>
    <!--<form role="form" action="patient_info.php">
        <div class="form-group">
            <label for="sel1">Select item per page (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage==5) echo "selected"?>>5</option>
                <option <?php if($itemPerPage==10) echo "selected"?>>10</option>
                <option <?php if($itemPerPage==15) echo "selected"?>>15</option>
                <option <?php if($itemPerPage==20) echo "selected"?>>20</option>
                <option <?php if($itemPerPage==25) echo "selected"?>>25</option>
            </select>
            <button type="submit">GO!</button>

        </div>
    </form>

    <form role="form" action="patient_info.php" method="post">
        <?php /*if(isset($_POST['filterByName'])) {
            $catchName = $_POST['filterByName'];
        }
        if(isset($_POST['filterByRoom'])) {
            $catchRoom = $_POST['filterByRoom'];
        }*/
        ?>
        <div class="form-group">
            <label>Filter by Name:</label>
            <input type="text" name="filterByName" value="<?php //if(isset($_POST['filterByName'])) {echo $catchName;}?>" id="title">
            <label>Filter by Room:</label>
            <input type="text" name="filterByRoom" value="<?php //if(isset($_POST['filterByRoom'])) {echo $catchRoom;}?>" id="description">
            <button type="submit">Submit!</button>
        </div>

    </form>
    <form role="form" action="patient_info.php" method="get">
        <?php //if(isset($_GET['search'])){
           // $catchSearch=$_GET['search'];
        //}
        ?>
        <div class="form-group">
            <label>Search:</label>
            <input type="text" name="search" value="<?php //if(isset($_GET['search'])){echo $catchSearch;}?>" id="search">
            <button type="submit">Search</button>
        </div>

    </form>-->
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Room</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach ($allInfo as $info){
                $sl++?>
                <tr>
                    <td><?php echo $sl+$pageStartFrom ?></td>
                    <td><?php echo $info->patient_id ?></td>
                    <td><?php echo $info->patient_name ?></td>
                    <td><?php echo $info->room_id ?></td>
                    <td><a href="release_patient_view.php?patient_id=<?php echo $info->patient_id ?>" class="btn btn-info" role="button">View</a>
                        <a href="released_delete.php?patient_id=<?php echo $info->patient_id ?>" class="btn btn-danger" role="button">Delete</a>
                        <a href="prescribe.php?patient_id=<?php echo $info->patient_id ?>" class="btn btn-success" role="button">Prescription</a>
                    </td>
                </tr>
            <?php }?>

            </tbody>
        </table>
        <center>
        <?php if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&&(empty($_GET['search']))) { ?>
            <ul class="pagination">
                <?php if($pageNo>1)
                {
                    $prev=$pageNo-1;
                    echo  "<li ><a href = 'released_patient.php?pageNo=$prev'>Prev</a ></li >";
                }
                ?>
                <?php echo $pagination ?>
                <?php if($pageNo<$noOfPage)
                {
                    $next=$pageNo+1;
                    echo "<li ><a href = 'released_patient.php?pageNo=$next'>Next</a ></li >";
                }
                ?>
            </ul>
        <?php } ?>
            </center>
    </div>
</div>
<footer class="text-center" style="margin-top: 215px">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
    $('#message').show().delay(3000).fadeOut();
</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo '"'.$commaSeparated.'"' ?>
        ];
        $( "#title" ).autocomplete({
            source: availableTags
        });
    } );
</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo '"'.$commaSeparatedString.'"' ?>
        ];
        $( "#description" ).autocomplete({
            source: availableTags
        });
    } );
</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $allTD ?>
        ];
        $( "#search" ).autocomplete({
            source: availableTags
        });
    } );
</script>

</body>
</html>

