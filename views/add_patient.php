<?php
include_once('../vendor/autoload.php');
use App\Doctor\Doctor;
use App\Utility\Utility;

$doctor= new Doctor();
$allItem=$doctor->doctorList();
$allRoom=$doctor->roomList();
//var_dump($allRoom);
//die();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>patient Entry</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--<style>
                tr:nth-child(even) {background: #ccc}
                tr:nth-child(odd) {background: #ccc}
            </style>-->
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="add_doctor.php">Add Doctor</a></li>
            <li><a href="patient_info.php">Doctor Info</a></li>
            <li><a href="patient_info.php">Patient Info</a></li>
            <li><a href="released_patient.php">Released Patient</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <h2 class="jumbotron" align="center" style="padding-top: 10px;padding-bottom: 10px;background-color: #11866f;color: #FFFFFF">Add Patient</h2>
    <div class="jumbotron" style="padding-right: 150px;padding-left: 150px;background-color: #11866f;color: #FFFFFF">
    <form role="form" action="patient_store.php" method="post">
        <div class="form-group">
            <label>Patient Name:</label>
            <input type="text" name="patient_name" class="form-control" placeholder="Enter Name">
        </div>
        <div class="form-group">
            <label>Patient Phone Number:</label>
            <input type="text" name="mobile" class="form-control" placeholder="Enter Phone">
        </div>
        <div class="form-group">
            <label>Disease:</label>
            <input type="text" name="disease" class="form-control" placeholder="Enter Disease">
        </div>
        <div class="form-group">
            <label for="sel1">Select Room:</label>
            <select class="form-control" id="sel1" name="room_id">
                <?php foreach($allRoom as $item){?>
                    <option value="<?php echo $item['room_id']?>"><?php echo $item['room_no']?></option>
                <?php } ?>
            </select>
        </div>

        <div class="form-group">
            <label>Entry Date:</label>
            <input type="text" name="entry_date" class="form-control" placeholder="Enter Date">
        </div>
        <div class="form-group">
            <label for="sel1">Select Doctor:</label>
            <select class="form-control" id="sel1" name="doctor_id">
                <?php foreach($allItem as $item){?>
                <option value="<?php echo $item['doctor_id']?>"><?php echo $item['doctor_name']?></option>
              <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="sel1">Select Your Gender:</label>
            <select class="form-control" id="sel1" name="gender">
                <option value="male">Male</option>
                <option value="female">Female</option>
                <option value="others">Others</option>
            </select>
        </div>
        <div class="form-group">
            <label>Age:</label>
            <input type="text" name="age" class="form-control" placeholder="Enter Age">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
    </div>
    <footer class="text-center" style="margin-top: 180px">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; PHP HUNTERS 2016
                    </div>
                </div>
            </div>
        </div>
    </footer>

</body>
</html>