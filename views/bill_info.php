<?php
include_once('../vendor/autoload.php');
use App\Patient\Patient;
use App\Doctor\Doctor;
use App\Message\Message;
use App\Utility\Utility;
$patient=new Patient();
$doctor=new doctor();
//var_dump($_POST);
//die();
$patient->prepare($_POST)->release_store();
$singlePatient=$patient->prepare($_POST)->view();
$Doctor_Id['doctor_id']=$singlePatient->doctor_id;
//var_dump($Doctor_Id);
//die();
$Room_Id['room_id']=$singlePatient->room_id;
$singleDoctor=$doctor->prepare($Doctor_Id)->view();
$singleRoom=$patient->prepare($Room_Id)->room_no();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <!--<style>
                tr:nth-child(even) {background: #ccc}
                tr:nth-child(odd) {background: #ccc}
            </style>-->
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="bill_pdf.php?patient_id:<?php echo $singlePatient->patient_id ?>">Download Bill</a></li>
            <li><a href="add_patient.php">Add Patient</a></li>
            <li><a href="patient_info.php">Doctors</a></li>
            <li><a href="patient_info.php">Patient Info</a></li>
            <li><a href="released_patient.php">Released Patient</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>
    <h2 class="jumbotron" align="center" style="padding-bottom: 10px;padding-top: 10px; background-color: dimgray;color:#FFFFFF"><?php echo $singlePatient->patient_name ?></h2>

    <div class="jumbotron" align="center" style="color: #000000;background-color: dimgray">
        <ul class="list-group" style="padding-left: 140px;padding-right: 150px">

            <li class="list-group-item">ID: <?php echo $singlePatient->patient_id ?></li>
            <li class="list-group-item">Name: <?php echo $singlePatient->patient_name ?></li>
            <li class="list-group-item">Mobile No: <?php echo $singlePatient->mobile ?></li>
            <li class="list-group-item">Disease: <?php echo $singlePatient->disease ?></li>
            <li class="list-group-item">Room No: <?php echo $singleRoom->room_no ?></li>
            <li class="list-group-item">Entry Date: <?php echo $singlePatient->entry_date ?></li>
            <li class="list-group-item">Assigned Doctor: <?php echo $singleDoctor->doctor_name ?></li>
            <li class="list-group-item">Gender: <?php echo $singlePatient->gender ?></li>
            <li class="list-group-item">Age: <?php echo $singlePatient->age ?></li>
            <li class="list-group-item">Release Date: <?php echo $singlePatient->release_date ?></li>
            <li class="list-group-item">Medicine Bill: <?php echo $singlePatient->medicine_bill ?></li>
            <li class="list-group-item">Room Bill: <?php echo $singlePatient->room_bill ?></li>
            <li class="list-group-item">Doctor Bill: <?php echo $singlePatient->doctor_bill ?></li>
            <li class="list-group-item">Total Bill: <?php echo $singlePatient->medicine_bill+$singlePatient->room_bill+$singlePatient->doctor_bill ?></li>
        </ul>
</div>
</div>
<footer class="text-center" style="margin-top: 180px">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
        $('#message').show().delay(3000).fadeOut();
    </script>

</body>
</html>