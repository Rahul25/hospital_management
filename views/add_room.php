<!DOCTYPE html>
<html lang="en">
<head>
    <title>add room</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Room</h2>
    <form role="form" action="store_room.php" method="post">
        <div class="form-group">
            <label>Room No:</label>
            <input type="text" name="room_no" class="form-control" value="" placeholder="Enter Room">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
