<!DOCTYPE html>
<html lang="en">
<head>
    <title>Prescription</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
    <!--<style>
                        tr:nth-child(even) {background: #ccc}
                        tr:nth-child(odd) {background: #ccc}
                    </style>-->
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="add_patient.php">Add Patient</a></li>
            <li><a href="patient_info.php">Doctor Info</a></li>
            <li><a href="patient_info.php">Patient Info</a></li>
            <li><a href="released_patient.php">Released Patient</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <h2 class="jumbotron" align="center" style="padding-top: 10px;padding-bottom: 10px;color: #FFFFFF;background-color: #11866f">Create Prescription</h2>

    <form role="form" action="store_prescription.php" method="post">
        <input type="hidden" name="patient_id"value="<?php echo $_GET['patient_id'] ?>">
        <div class="form-group">
            <label>Enter Prescription:</label>
            <textarea class="form-control" rows="5" id="prescription" name="prescription"></textarea>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
<footer class="text-center" style="margin-top: 180px">
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
    tinymce.init({
        selector: '#prescription'
    });
</script>
</body>
</html>