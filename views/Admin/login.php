<?php
session_start();
include_once ('../../vendor/autoload.php');
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;
use App\Admin\Auth;


$auth= new Auth();
$status=$auth->prepare($_POST)->is_registered();
if($status){
    $_SESSION['user_email']= $_POST['email'];
    return Utility::redirect('../welcome_admin.php');

}

else{
    Message::message("<div class=\"alert alert-danger\">
  <strong>Taken!</strong> Please submit correct email or password
</div>");
    return Utility::redirect('../../index.php');
}



