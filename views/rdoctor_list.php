<?php
//var_dump($_GET);

include_once('../vendor/autoload.php');
//use App\Patient\Patient;
use App\Admin\Auth;
use App\Utility\Utility;
//$patient= new Patient();
$auth= new Auth();
$allDoctor=$auth->prepare($_GET)->doctor_list();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>doctor list</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!--<style>
                            tr:nth-child(even) {background: #ccc}
                            tr:nth-child(odd) {background: #ccc}
                        </style>-->
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="add_doctor.php">Add Doctor</a></li>
            <li><a href="add_patient.php">Add Patient</a></li>
            <li><a href="patient_info.php">Patient Info</a></li>
            <li><a href="doctor_info.php">Doctors</a></li>
            <li><a href="doctor_info.php">Released Patient</a></li>
    </div>
    </form></li>

    </ul>
    </div>
</nav>

<div class="container">
    <h2 class="jumbotron" align="center" style="padding-bottom: 10px;padding-top: 10px; background-color: dimgray;color:#FFFFFF"><?php echo $allDoctor[0]['dept_name']?></h2>

    <div class="jumbotron" style="color: #000000;background-color: dimgray">

        <?php $sl=1;
        foreach($allDoctor as $doctor) { ?>
            <ul class="list-group">
                <li class="list-group-item" style="background-color: #11866f;color: #FFFFFF"><?php echo $sl++ ?>: <?php echo $doctor['doctor_name'] ?></li>

            </ul>
        <?php } ?>
    </div>
</div>
<footer class="text-center" style="margin-top: 180px">
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
</html>

