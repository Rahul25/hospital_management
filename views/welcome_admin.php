<?php
session_start();
include_once ('../vendor/autoload.php');
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;
use App\Admin\Auth;
$ob= new App\Admin\Auth();
$item=$ob->menuList();

$auth= new Auth();
$status=$auth->is_loggedin();
if($status== FALSE){
    Message::message("<div class=\"alert alert-danger\">
<strong>Taken!</strong> You have to log in before view this page
</div>");
    return Utility::redirect('../index.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hospital Management System</title>
    <link rel="stylesheet" href="../Resources/bootstrap/css/bootstrap.css">

    <!-- Bootstrap Core CSS -->
    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]--></head>
<body id="page-top" class="index">

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->

        <!--     <h1 align="center" color="#fff">Hospital Management System</h1>-->
        <div class="navbar-header page-scroll">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>



            </button>
            <a class="navbar-brand" style="margin-top: -10px;margin-left: -69px"href="#page-top"><img src="../Resources/welcome/img/logo.PNG" width="100" height="40"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" id="navbarmenu">
                <li class="page-scroll">
                    <a href="all_doctor.php">All Doctors</a>
                </li>
                <li class="page-scroll">
                    <a href="all_patient.php">All InPatients</a>
                </li>
                <li class="page-scroll">
                    <a href="all_released.php">All Released</a>
                </li>
                <!--<li class="page-scroll">
                    <a href="view_profile.php?email=<?php //echo $_SESSION['user_email'] ?>"><?php //echo $item[3]['menus']?><?php //echo $_SESSION['user_email'] ?>"><?php //echo $item[3]['menus']?></a>
                </li>-->
                <li class="page-scroll">
                    <a href="welcome.php">Visit Site</a>
                </li>
                <li class="page-scroll">
                    <a href="Authenticate/logout.php">Log Out</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Header -->
<header style="min-height:870px">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="../Resources/welcome/img/profile.png" alt="">
                <div class="intro-text" style="margin-top: 105px;">
                    <span class="name" style="margin-top:50px">Welcome To Our Hospital</span>
                    <hr class="star-light" style="margin-top: 80px;">


                    <div class="row" id="margingTop" style="margin-top:50px">
                        <div class="col-lg-12">
                            <a href="dashboard.php" class="btn btn-success">View All</a>
                            <a href="menu_list.php" class="btn btn-success">Menu List</a>
                            <a href="dept_list.php" class="btn btn-success">Department List</a>
                            <a href="room_list.php" class="btn btn-success">Room List</a>
                            <a href="receptionist_info.php" class="btn btn-success">Control User </a>

                        </div>
                    </div>





















                    <!--<span class="skills">Best Treatment - Health & Life Service - Qualified Medical Service</span>-->
                </div>
            </div>
        </div>
    </div>
</header>





<footer class="text-center">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

</body>


</html>