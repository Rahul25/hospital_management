<!DOCTYPE html>
<html lang="en">
<head>
    <title>add dept</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Department</h2>
    <form role="form" action="store_dept.php" method="post">
        <div class="form-group">
            <label>Depatment Name:</label>
            <input type="text" name="dept_name" class="form-control" value="" placeholder="Enter dept">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
