-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2016 at 07:10 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'sharadmohajan@gmail.com', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
`dept_id` int(11) NOT NULL,
  `dept_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dept_id`, `dept_name`) VALUES
(1, 'cardiology'),
(2, 'neurology'),
(3, 'orthopedix');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE IF NOT EXISTS `doctor` (
`doctor_id` int(11) NOT NULL,
  `doctor_name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dept_id` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`doctor_id`, `doctor_name`, `mobile`, `email`, `dept_id`) VALUES
(4, 'anwar', '01819785928', 'sharadmohajan305@gmail.com', 2),
(5, 'sharad', '01627837641', 'sharadmohajan30oi@gmail.com', 2),
(6, 'tanvir', '01829757110', 'sharadmohajan08@gmail.com', 1),
(7, 'jahidul alam', '01836746523', 'sharadmohajan@gmail.com', 3),
(8, 'sharad mohajan', '0183674652', 'sharad@gmail.com', 1),
(9, 'anwar ullah', '01836746523', 'sharad@gmail.com', 3),
(10, '', '', '', 2),
(11, 'giyas', '01836746523', 'a@gmail.com', 3),
(12, 'giyas', '01836746523', 'a@gmail.com', 3);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
`id` int(11) NOT NULL,
  `menus` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menus`, `link`) VALUES
(1, 'Doctor Info', 'doctor_info.php'),
(2, 'InPatient', 'patient_info.php'),
(3, 'Released Patient', 'released_patient.php'),
(4, 'Profile', 'view_profile.php'),
(5, 'Log Out', 'Authenticate/logout.php');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
`patient_id` int(11) NOT NULL,
  `patient_name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `disease` varchar(255) NOT NULL,
  `room_id` int(11) NOT NULL,
  `seat` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `doctor_id` int(255) NOT NULL,
  `release_date` date NOT NULL,
  `medicine_bill` int(11) NOT NULL,
  `doctor_bill` int(11) NOT NULL,
  `room_bill` int(11) NOT NULL,
  `is_active` int(11) DEFAULT NULL,
  `gender` text NOT NULL,
  `age` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patient_id`, `patient_name`, `mobile`, `disease`, `room_id`, `seat`, `entry_date`, `doctor_id`, `release_date`, `medicine_bill`, `doctor_bill`, `room_bill`, `is_active`, `gender`, `age`) VALUES
(3, 'anwar ullah', '0183674652', 'hand pain', 4, 3, '2016-02-05', 5, '2016-09-01', 600, 100, 200, 1, 'female', 23),
(4, 'tanvir', '01627837641', 'headache', 1, 2, '2016-08-01', 6, '2016-09-01', 1000, 1000, 100, 1, 'female', 21),
(5, 'cv', '01819785928', 'df', 4, 1, '2016-08-01', 4, '2016-09-01', 500, 500, 500, 1, 'female', 23),
(7, 'anwar', '01829757110', 'headache', 4, 1, '2016-08-01', 5, '2016-09-01', 600, 200, 100, 1, 'female', 21),
(10, 'jahid', '5654657', 'dggh', 1, 1, '0000-00-00', 4, '0000-00-00', 0, 0, 0, 1, '', 25),
(11, 'kabir', '54458', 'dhgfhg', 2, 1, '0000-00-00', 6, '2016-05-05', 1000, 500, 1000, 1, 'male', 30),
(12, 'gf', '01829757110', 'fever', 6, 1, '2016-08-01', 5, '2016-09-01', 100, 100, 100, 1, 'female', 24),
(13, 'jitu', '018249564356', 'fever', 2, 0, '2016-01-01', 7, '0000-00-00', 0, 0, 0, NULL, 'male', 25),
(14, 'habib', '018249564356', 'heart break', 2, 0, '2016-01-01', 4, '0000-00-00', 0, 0, 0, NULL, 'male', 23),
(15, '', '', '', 2, 0, '0000-00-00', 4, '0000-00-00', 0, 0, 0, NULL, 'male', 0),
(16, 'sarad', '018249564356', 'fever', 1, 0, '2016-01-01', 6, '2016-05-05', 100, 100, 100, 1, 'male', 23);

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE IF NOT EXISTS `prescription` (
`prescription_id` int(11) NOT NULL,
  `drugs` varchar(1024) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `drugswithouthtml` varchar(1024) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`prescription_id`, `drugs`, `patient_id`, `drugswithouthtml`) VALUES
(9, '<p>Morning:</p>\r\n<p>1.para</p>\r\n<p>Noon:</p>\r\n<p>1.ors</p>', 6, 'Morning:\r\n1.para\r\nNoon:\r\n1.ors'),
(10, '<p><strong>Morning:</strong></p>\r\n<p><strong>1.para</strong></p>\r\n<p><strong>2.orc</strong></p>', 12, 'Morning:\r\n1.para\r\n2.orc'),
(11, '<p>Morning:para</p>\r\n<p>Noon:napa</p>\r\n<p>evening:saclo</p>', 13, 'Morning:para\r\nNoon:napa\r\nevening:saclo'),
(12, '<p>paracitamal</p>', 16, 'paracitamal');

-- --------------------------------------------------------

--
-- Table structure for table `receptionist`
--

CREATE TABLE IF NOT EXISTS `receptionist` (
`receptionist_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_block` int(11) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `shift` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `age` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receptionist`
--

INSERT INTO `receptionist` (`receptionist_id`, `first_name`, `last_name`, `email`, `password`, `is_block`, `full_name`, `image`, `shift`, `mobile`, `age`) VALUES
(1, 'sharad', 'mohajan', 'sharadmohajan@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'sharad Mohajan', '1469710127154665_411337735601453_121538839_n.jpg', 'morning', '01829757110', 23),
(2, 'anwar', 'hossen', 'sharad@gmail.com', 'c99d31729b10fea4cfda6a5a92fc43da', NULL, 'Anwar ullah', '', 'night', '01829757110', 25),
(3, 'tanvir', 'hossen', 'sharad1@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '', '', '', '', 0),
(4, 'tanu', 'hossen', 'sharad2@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '', '', '', '', 0),
(5, 'fanu', 'hossen', 'sharad3@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '', '', '', '', 0),
(6, 'tonu', 'jh', 'sharad4@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '', '', '', '', 0),
(7, 'jitu', 'hasan', 'hasan@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '', '', '', '', 0),
(8, 'anwar', 'hossen', 'a@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
`room_id` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `is_empty` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`room_id`, `room_no`, `is_empty`) VALUES
(1, 101, NULL),
(2, 102, 1),
(3, 103, 1),
(4, 104, NULL),
(5, 105, 1),
(6, 107, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
 ADD PRIMARY KEY (`doctor_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
 ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `prescription`
--
ALTER TABLE `prescription`
 ADD PRIMARY KEY (`prescription_id`);

--
-- Indexes for table `receptionist`
--
ALTER TABLE `receptionist`
 ADD PRIMARY KEY (`receptionist_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
 ADD PRIMARY KEY (`room_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
MODIFY `doctor_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `prescription`
--
ALTER TABLE `prescription`
MODIFY `prescription_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `receptionist`
--
ALTER TABLE `receptionist`
MODIFY `receptionist_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
