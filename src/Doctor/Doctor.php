<?php

namespace App\Doctor;
//session_start();
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
class Doctor extends DB{
    public $doctor_id;
    public $doctor_name="";
    public $mobile="";
    public $email="";
    public $dept_id="";
    public $search="";
    public $filterByName="";
    public $filterByDepartment="";
    public function __construct()
    {
        parent::__construct();
    }


    public function prepare($data=Array()){
        if(array_key_exists('doctor_id',$data)) {
            $this->doctor_id = $data['doctor_id'];
            //echo $this->patient_id;
            //die();
        }
        if(array_key_exists('doctor_name',$data)) {
            $this->doctor_name = $data['doctor_name'];
            //echo  $this->doctor_name;
            //die();
        }
        if(array_key_exists('mobile',$data)) {
            $this->mobile = $data['mobile'];
        }
        if(array_key_exists('email',$data)) {
            $this->email= $data['email'];
        }
        if(array_key_exists('dept_id',$data)) {
            $this->dept_id = $data['dept_id'];
            //echo $this->dept_id;
            //die();
        }
        if(array_key_exists('search',$data)) {
            $this->search= $data['search'];
        }
        if(array_key_exists('filterByName',$data)) {
            $this->filterByName= $data['filterByName'];
        }
        if(array_key_exists('filterByDepartment',$data)) {
            $this->filterByDepartment= $data['filterByDepartment'];
        }
        return $this;

    }

    public function store(){
        $query="INSERT INTO `hms`.`doctor` (`doctor_name`, `mobile`, `email`,`dept_id`) VALUES ('".$this->doctor_name."', '".$this->mobile."', '".$this->email."', '".$this->dept_id."')";
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> You are successfully stored.
</div>");
            Utility::redirect('welcome.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has been stored Patient Data successfully.
</div>");
            Utility::redirect('welcome.php');

        }

    }


    public function view(){
        $query="SELECT * FROM `doctor` WHERE `doctor_id` =".$this->doctor_id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }
    public function single_dept(){
        $query="SELECT * FROM `department` WHERE `dept_id` =".$this->dept_id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }

    public function update(){
        $query="UPDATE `hms`.`doctor` SET `doctor_id` = '".$this->doctor_id."',`doctor_name` = '".$this->doctor_name."',`mobile` = '".$this->mobile."',`dept_id` = '".$this->dept_id."',`email` = '".$this->email."' WHERE `doctor`.`doctor_id` = ".$this->doctor_id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been updated successfully.
</div>");
            Utility::redirect('doctor_info.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('doctor_info.php');

        }

    }
    public function delete()
    {
        $query = "DELETE FROM `hms`.`doctor` WHERE `doctor`.`doctor_id` =" . $this->doctor_id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect('doctor_info.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect('doctor_info.php');


        }
    }

    public  function index(){
        $_allItem=array();
        $whereClause= " 1=1 ";
        if(!empty($this->filterByName)) {
            $whereClause .= " AND doctor_name LIKE '%".$this->filterByName."%'";
        }
        if(!empty($this->filterByDepartment)){
            $whereClause .= " AND department LIKE '%".$this->filterByDepartment."%'";
        }
        if(!empty($this->search)){
            $whereClause .= " AND doctor_name LIKE '%".$this->search."%'";
        }
        $query= "SELECT * FROM `doctor` WHERE".$whereClause;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }
    public  function doctorList()
    {
        $_allItem=array();
        $query= "SELECT * FROM `doctor`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }
    public  function roomList()
    {
        $_allItem=array();
        $query= "SELECT * FROM `room`WHERE `is_empty` IS NULL ";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }
    public  function patient_list()
    {
        $_allItem=array();
        $query= "SELECT p.patient_name,d.doctor_name FROM patient AS p INNER JOIN doctor AS d ON p.doctor_id=d.doctor_id WHERE d.doctor_id=".$this->doctor_id;
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `doctor`";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allItem=array();
        $query="SELECT * FROM `doctor` LIMIT ".$pageStartFrom.",".$Limit;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;

    }
    public function allName(){
        $_allItem= array();
        $query="SELECT doctor_name FROM `doctor`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row['doctor_name'];
        }
        return $_allItem;
    }
        public function allDepartment(){
        $_allItem= array();
        $query="SELECT dept_name FROM `department`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row['dept_name'];
        }
        return $_allItem;
    }
    public  function deptList()
    {
        $_allItem=array();
        $query= "SELECT * FROM `department`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }
}


