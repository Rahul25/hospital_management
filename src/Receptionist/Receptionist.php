<?php

namespace App\Receptionist;
//session_start();
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
class Receptionist extends DB
{
    public $receptionist_id="";
    public $receptionist_name = "";
    public $mobile = "";
    public $password="";
    public $email = "";
    public $shift = "";
    public $image = "";
    public $age = "";

    public function __construct()
    {
        parent::__construct();
    }


    public function prepare($data = Array())
    {
        if (array_key_exists('receptionist_id', $data)) {
            $this->receptionist_id = $data['receptionist_id'];
            //echo $this->patient_id;
            //die();
        }
        if (array_key_exists('receptionist_name', $data)) {
            $this->receptionist_name = $data['receptionist_name'];
            //echo  $this->doctor_name;
            //die();
        }
        if (array_key_exists('mobile', $data)) {
            $this->mobile = $data['mobile'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('shift', $data)) {
            $this->shift = $data['shift'];
            //echo $this->dept_id;
            //die();
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }
        if (array_key_exists('age', $data)) {
            $this->age = $data['age'];
        }
        return $this;

    }

    public function store()
    {
        $query = "UPDATE `hms`.`receptionist` SET `full_name` = '{$this->receptionist_name}',`mobile` = '{$this->mobile}',`image` = '{$this->image}',`shift` = '{$this->shift}',`age`='{$this->age}' WHERE `receptionist`.`email` ='".$this->email."'";
       //echo $query;
       // die();
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> You are successfully stored.
</div>");
            Utility::redirect('welcome.php');

        } else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong>Profile Data has not been stored successfully.
</div>");
            Utility::redirect('welcome.php');

        }

    }


    public function view()
    {
        $query = "SELECT * FROM `receptionist` WHERE `email` ='".$this->email."'";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `hms`.`receptionist` SET `full_name` = '".$this->receptionist_name."',`password` = '".$this->password."',`mobile` = '".$this->mobile."',`email` = '".$this->email."',`shift` = '".$this->shift."',`image` = '".$this->image."',`age` = '".$this->age."' WHERE `receptionist`.`receptionist_id` = ".$this->receptionist_id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been updated successfully.
</div>");
            Utility::redirect('../index.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('../index.php');

        }

    }
}