<?php session_start();
include_once('vendor/autoload.php');
use App\Message\Message;
use App\Utility\Utility;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Medical Center</title>
<link href="Resources/css.css" rel="stylesheet" type="text/css" />
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="Resources/css/normalize.css">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


  <link rel="stylesheet" href="Resources/css/style.css">

</head>

<body>
<div id="message">
  <?php
    if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
      echo Message::message();
    }
    ?>
</div>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="background-image:url(Resources/images/index_02.gif); background-repeat:repeat-x; background-position:top;"><table width="724" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" valign="top"><table width="724" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="64" align="left" valign="top"><img src="Resources/images/index_04.gif" width="170" height="64" alt="" /></td>
                <td width="660" align="left" valign="bottom" style="padding-left:2px;"><table width="539" border="0" cellspacing="0" cellpadding="0">
                    <!--<tr>
                      <td width="90" align="center" valign="middle" class="menu"><a href="index.html">Home</a></td>
                      <!--<td width="90" align="center" valign="middle" class="menu"><a href="content.html">Add patient</a></td>
                      <td width="90" align="center" valign="middle" class="menu"><a href="content.html">Release Patient</a></td>
                      <td width="90" align="center" valign="middle" class="menu"><a href="content.html">Add Doctor </a></td>-->
                     <!-- <td width="90" align="center" valign="middle" class="menu"><a href="content.html">Doctor Info</a></td>
                      <td width="90" align="center" valign="middle" class="menu"><a href="contact.html">Patient Info</a></td>
					  <td width="90" align="center" valign="middle" class="menu"><a href="contact.html">Log Out</a></td>
                    </tr>-->
                </table></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="3%" align="left" valign="top" bgcolor="#FFFFFF" style="background-image:url(Resources/images/index_19.gif); background-repeat:repeat-y; background-position:left;"><img src="Resources/images/index_09.gif" width="24" height="10" alt="" /></td>
                <td width="30%" rowspan="2" align="left" valign="top"><img src="Resources/images/index_10.gif" width="218" height="213" alt="" /></td>
                <td width="19%" rowspan="2" align="left" valign="top"><img src="Resources/images/index_11.gif" width="136" height="213" alt="" /></td>
                <td width="44%" rowspan="2" align="left" valign="top"><table width="97%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="left" valign="top"><img src="Resources/images/index_12.gif" width="316" height="53" alt="" /></td>
                    </tr>
					<!--<tr>
                      <td align="left" valign="top"><img src="images/index_25.gif" width="316" height="53" alt="" /></td>
                    </tr>
					<tr>
                      <td align="left" valign="top"><img src="images/bag.gif" width="316" height="110" alt="" /></td>
                    </tr>-->
					
                    <tr>
                      <td align="left" valign="top"><a href="index.php"><img src="Resources/images/hms.gif" alt="" width="316" height="58" border="0" /></a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><img src="Resources/images/index_18.gif" width="316" height="102" alt="" /></td>
                    </tr>
                </table></td>
                <td width="4%" align="left" valign="top" bgcolor="#FFFFFF" style="background-image:url(Resources/images/index_23.gif); background-repeat:repeat-y; background-position:right;"><img src="Resources/images/index_13.gif" width="30" height="10" alt="" /></td>
              </tr>
              <tr>
                <td align="left" valign="top" bgcolor="#FFFFFF" style="background-image:url(Resources/images/index_19.gif); background-repeat:repeat-y; background-position:left;">&nbsp;</td>
                <td width="4%" align="left" valign="top" bgcolor="#FFFFFF" style="background-image:url(Resources/images/index_21.gif); background-repeat:repeat-y; background-position:right;">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="10" align="left" valign="top" style="background-image:url(Resources/images/index_19.gif); background-repeat:repeat-y; background-position:left;">&nbsp;</td>
                    <td align="left" valign="top" style="padding-top:30px; padding-bottom:6px;"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="68%" align="left" valign="top" style="padding-left:10px;"><table width="92%" border="0" align="left" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="left" valign="top"><table width="99%" border="0" cellspacing="0" cellpadding="0">

                             <!-- <tr>
                                <td align="right" valign="top" style="padding-bottom:25px;"><img src="images/index_25.gif" width="260" height="50" alt="" /></td>
                              </tr>
							  
							  
							  <tr>
							  <td width="90" align="center" valign="middle" class="denu"><a href="content.html">Add patient</a></td>
							  </tr>
							  <tr>
                               <td width="90" align="center" valign="middle" class="denu"><a href="content.html">Release Patient</a></td>
					          </tr>
							  <tr>
                            <td width="90" align="center" valign="middle" class="denu"><a href="content.html">Add Doctor </a></td>
						       </tr>
							   <tr>
                            <td width="90" align="center" valign="middle" class="denu"><a href="content.html">Released patient Info </a></td>
						       </tr>
							 
                              <!--<tr>
                                <td align="left" valign="top" class="body"><p><span style="color:#009604;"><strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</strong></span> Nullam quis turpis eu libero varius vestibulum. In feugiat. Sed et turpis ac risus aliquet rhoncus. Nam cursusturpis eu libero varius vestibulum.<br />
                                  <br />
                                    <a href="content.html">read more</a></p>
                                  </td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td align="left" valign="top"><table width="99%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td align="left" valign="top" style="padding-top:27px; padding-bottom:26px;"><img src="images/index_41.gif" width="141" height="21" alt="" /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td align="left" valign="top" style="padding-bottom:22px;"><table width="93%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td width="40%" align="left" valign="top"><img src="images/index_46.gif" width="64" height="48" alt="" /></td>
                                        <td width="60%" align="left" valign="top" class="body1"><a href="content.html">Lorem ipsum dolor</a> <br />
                                          sitamet,consectetuer adipiscingelit. Nullam</td>
                                      </tr>
                                    </table></td>
                                    <td align="left" valign="top"><table width="93%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td width="40%" align="left" valign="top"><img src="images/index_49.gif" width="64" height="48" alt="" /></td>
                                        <td width="60%" align="left" valign="top" class="body1"><a href="content.html">Lorem ipsum dolor</a><br />
                                          sitamet,consectetuer adipiscingelit. Nullam</td>
                                      </tr>
                                    </table></td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="top"><table width="93%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td width="40%" align="left" valign="top"><img src="images/index_53.gif" width="64" height="48" alt="" /></td>
                                        <td width="60%" align="left" valign="top" class="body1"><a href="content.html">Lorem ipsum dolor</a> <br />
                                          sitamet,consectetuer adipiscingelit. Nullam</td>
                                      </tr>
                                    </table></td>
                                    <td align="left" valign="top"><table width="93%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td width="40%" align="left" valign="top"><img src="images/index_54.gif" width="64" height="48" alt="" /></td>
                                        <td width="60%" align="left" valign="top" class="body1"><a href="content.html">Lorem ipsum dolor</a> <br />
                                          sitamet,consectetuer adipiscingelit. Nullam</td>
                                      </tr>
                                    </table></td>
                                  </tr>
                                </table></td>
                              </tr>
                              <tr>
                                <td align="left" valign="top" style="padding-top:30px;"><img src="images/index_58.gif" width="313" height="44" alt="" /></td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                        <td width="32%" align="left" valign="top"><table width="95%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td align="left" valign="top" style="padding-bottom:10px;"><a href="content.html"><img src="images/index_28.gif" alt="" width="207" height="97" border="0" /></a></td>
                          </tr>
                          <tr>
                            <td align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F7F7F9">
                              <tr>
                                <td width="10" height="10" align="left" valign="top"><img src="images/index_33.gif" width="10" height="10" alt="" /></td>
                                <td width="91%" align="left" valign="top">&nbsp;</td>
                                <td width="12" height="12" align="right" valign="top"><img src="images/index_35.gif" width="12" height="12" alt="" /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="top">&nbsp;</td>
                                <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td align="left" valign="top" style="padding-bottom:15px;"><img src="images/index_39.gif" width="180" height="22" alt="" /></td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="top" class="body" style="padding-bottom:25px;"><span style="color:#009604;"><strong>10-JAN-2007</strong></span><br />
<br />
Quis turpis eu libero varius vestibulum. In feugiat. Sed et turpis ac risus aliquet rhoncus.<br />
<br />
<a href="content.html">read more</a></td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="top" class="body"><span style="color:#009604;"><strong>10-MAR-2007</strong></span><br />
<br />
Quis turpis eu libero varius vestibulum. In feugiat. Sed et turpis ac risus aliquet rhoncus.<br />
<br />
<a href="content.html">read more</a></td>
                                  </tr>
                                </table></td>-->
								<div class="form">
                                  <button type="submit" onclick="window.location.href='adminSignup.php'" class="button button-block"/>Admin Panel</button>


                                  <ul class="tab-group">
        <li class="tab active"><a href="#signup">Sign Up</a></li>
        <li class="tab"><a href="#login">Log In</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="signup">   
          <h1>Sign Up for Receptionist</h1>
          
          <form action="views/Authenticate/registration.php" method="post">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                First Name<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" name="first_name" />
            </div>
        
            <div class="field-wrap">
              <label>
                Last Name<span class="req">*</span>
              </label>
              <input type="text"required autocomplete="off" name="last_name"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" name="email"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" name="password"/>
          </div>
          
          <button type="submit" class="button button-block"/>Get Started</button>
          
          </form>

        </div>
        
        <div id="login">   
          <h1>Welcome Back!</h1>
          
          <form action="views/Authenticate/login.php" method="post">
          
            <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" name="email"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" name="password"/>
          </div>
          
         <!-- <p class="forgot"><a href="#">Forgot Password?</a></p>-->
          
          <button class="button button-block"/>Log In</button>
          
          </form>

        </div>
        
      </div><!-- tab-content -->
      
</div> <!-- /form -->
   

      

    
                                <td align="left" valign="top">&nbsp;</td>
                              </tr>
                              <tr>
                                <td width="12" height="12" align="left" valign="bottom"><img src="Resources/images/index_60.gif" width="12" height="12" alt="" /></td>
                                <td align="left" valign="top">&nbsp;</td>
                                <td width="12" height="12" align="right" valign="bottom"><img src="Resources/images/index_62.gif" width="12" height="12" alt="" /></td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table></td>
                    <td width="10" align="right" valign="top" style="background-image:url(Resources/images/index_23.gif); background-repeat:repeat-y; background-position:right;">&nbsp;</td>
                  </tr>
                  
                </table></td>
              </tr>
              <tr>
                <td height="10" align="left" valign="bottom"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="10" height="10" align="left" valign="bottom"><img src="Resources/images/index_66.gif" width="10" height="10" alt="" /></td>
                    <td width="704" align="left" valign="top" style="background-image:url(Resources/images/index_67.gif); background-repeat:repeat-x; background-position:bottom;"> </td>
                    <td width="10" height="10" align="right" valign="bottom"><img src="Resources/images/index_69.gif" width="10" height="10" alt="" /></td>
                  </tr>
                </table></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>

      <tr>
        <td align="left" valign="top"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
          <!--<tr>
            <td align="center" valign="top" style="padding-top:20px; padding-bottom:13px;"><pre class="footer"><a href="index.html">Home</a>     |     <a href="content.html">About us</a>     |     <a href="content.html">Service</a>     |     <a href="content.html">Patients</a>     |     <a href="content.html">Advice</a>     |     <a href="contact.html">Contact us</a></pre></td>
          </tr>-->
          <tr>
            <td align="center" valign="top" class="copyright" style="padding-bottom:19px;">Copyright © PHP Hunters. All rights reserved</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="Resources/js/index.js"></script>
<script>
  $('#message').show().delay(3000).fadeOut();
</script>
</body>
</html>